<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\MyController@index' )-> name('home')->middleware('auth');
Route::get('/create', 'App\Http\Controllers\MyController@create' )-> name('create')->middleware('auth');
Route::post('/create', 'App\Http\Controllers\MyController@store' )-> name('store')->middleware('auth');
Route::get('/edit{id}', 'App\Http\Controllers\MyController@edit' )-> name('edit')->middleware('auth');
Route::post('/update{id}', 'App\Http\Controllers\MyController@update' )-> name('update')->middleware('auth');
Route::delete('/delete{id}', 'App\Http\Controllers\MyController@delete' )-> name('delete')->middleware('auth');






Route::get('/register', 'App\Http\Controllers\RegistrationController@create')-> name('register');
Route::post('/register', 'App\Http\Controllers\RegistrationController@store')-> name('regiseruser');
 
Route::get('/login', 'App\Http\Controllers\SessionsController@create')->name('loginc');
Route::post('/login', 'App\Http\Controllers\SessionsController@store')->name('logincv');
Route::get('/logout', 'App\Http\Controllers\SessionsController@destroy')->name('destroy');

Route::get('/product', 'App\Http\Controllers\ProductController@index' )-> name('product')->middleware('auth');
Route::post('/product', 'App\Http\Controllers\ProductController@store' )-> name('productstore')->middleware('auth');
Route::get('/proedit{id}', 'App\Http\Controllers\ProductController@edit' )-> name('productedit')->middleware('auth');
Route::post('/proupdate{id}', 'App\Http\Controllers\ProductController@update' )-> name('productupdate')->middleware('auth');
Route::delete('/prodelete{id}', 'App\Http\Controllers\ProductController@delete' )-> name('productdelete')->middleware('auth');




Route::get('/hello', function () {
    return "hii";
})
;
