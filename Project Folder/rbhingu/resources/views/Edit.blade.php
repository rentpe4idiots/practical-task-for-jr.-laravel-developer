@extends('layouts.main')
 @section('content')
 <div class= "container">
 @if($errors->any())
 @foreach($errors ->all() as $error)
 <div class="alert alert-danger" role="alert">
 {{ $error }}
</div>
@endforeach
 @endif
     <h3>edit </h3><br>
    <!-- Default form register -->
<form class="text-center border border-light p-5" action="{{ route('update' , $customer->id) }}" method="POST" >
{{ csrf_field() }}

<p class="h4 mb-4">edit</p>

<div class="form-row mb-4">
    <div class="col">
        <!-- First name -->
        <input type="text" id="defaultRegisterFormFirstName" name="Name" 
        value="{{ $customer->customerName }}" class="form-control" placeholder="Name of Customer">
    </div>      
    
</div>

<!-- E-mail -->
<input type="email" id="defaultRegisterFormEmail" name="Email" class="form-control mb-4" value="{{ $customer->customerEmail }}" placeholder="E-mail">


<!-- Phone number -->
<input type="text" id="defaultRegisterPhonePassword" class="form-control" name="PhoneNo" value="{{ $customer->phoneNo }}"  placeholder="Phone number" aria-describedby="defaultRegisterFormPhoneHelpBlock">


<!-- Sign up button -->
<button class="btn btn-info my-4 btn-block" type="submit">Update</button>



</form>
</div>
<br>
<!-- Default form register -->
 @endsection