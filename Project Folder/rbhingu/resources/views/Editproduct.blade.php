@extends('layouts.main')
 @section('content')
 <div class= "container">
 @if($errors->any())
 @foreach($errors ->all() as $error)
 <div class="alert alert-danger" role="alert">
 {{ $error }}
</div>
@endforeach
 @endif
 
    <!-- Default form register -->
    <form class="text-center border border-light p-5" action="{{ route('productupdate' , $product->id) }}" method="POST" >
{{ csrf_field() }}

<p class="h4 mb-4">Sign up </p>

<div class="form-row mb-4">
    <div class="col">
        <!-- sku -->
        <input type="text" id="defaultRegisterFormFirstName" name="sku" value="{{ $product->sku }}" class="form-control" placeholder="Stock Keeping Unit">
    </div>      
    

        <!--name -->
        <div class="col">
        <input type="text" id="defaultRegisterFormEmail" name="name" value="{{ $product->Name }}" class="form-control mb-4" placeholder="Name">
        </div>
</div>
<div class="form-row mb-4">
<!-- category -->
    <div class="col">
        <input type="text" id="defaultRegisterPhonePassword" class="form-control" name="price" value="{{ $product->Price }}" placeholder="Price" aria-describedby="defaultRegisterFormPhoneHelpBlock">
    </div>


        <!-- category -->
    <div class="col">
    <input type="text" id="defaultRegisterPhonePassword" class="form-control" name="category" value="{{ $product->Category }}"placeholder="Category" aria-describedby="defaultRegisterFormPhoneHelpBlock">
    </div>
</div>

<div class="form-row mb-4">
    <div class="col">
    <input type="text" id="defaultRegisterPhonePassword" class="form-control" name="qty" value="{{ $product->Qty }}" placeholder="Quantity" aria-describedby="defaultRegisterFormPhoneHelpBlock">
    </div>

    <div class="col">
    <input type="file" id="defaultRegisterPhonePassword" class="form-control" name="image" value="{{ $product->image }}" placeholder="Image" aria-describedby="defaultRegisterFormPhoneHelpBlock">
    </div>
</div>   
<!-- Sign up button -->
<button class="btn btn-info my-4 btn-block" type="submit">Update Product</button>



</form>
</div>
<br>
<!-- Default form register -->
 @endsection