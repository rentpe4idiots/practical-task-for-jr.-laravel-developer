 @extends('layouts.main')
 @section('content')
 
 <div class="container">
 

     <h1> Home</h1>
     @if(session ('message'))
     <div class="alert alert-success" role="alert">
     {{ session ('message') }}
    </div>
     @endif
     <table class="table">
  <thead class="black white-text">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Customer Name</th>
      <th scope="col">email</th>
      <th scope="col">PhoneNo</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($customers as $customer)
    <tr>
      <th scope="row">{{ $customer->id }}</th>
      <td>{{ $customer->customerName }}</td>
      <td>{{ $customer->customerEmail }}</td>
      <td>{{ $customer->phoneNo }}</td>
      <td><a class="btn btn-raised btn-primary btn-sm" href="{{ route('edit',$customer->id) }} ">
       <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
    
    <form method="post" id="delete-form-{{ $customer->id }}" action="{{ route('delete',$customer->id) }}" 
    style="display: none; "> 
    {{ csrf_field() }}
    {{ method_field('delete') }}
    </form>
    <button onclick="if (confirm('Are you sure to delete?')){
        event.preventDefault();
        document.getElementById('delete-form-{{$customer->id}}').submit();
    }else{
        event.preventDefault();
    }
   

    "   class="btn btn-raised btn-danger btn-sm" href="  "><i class="fa fa-trash" aria-hidden="true">
       </i>
       </button>
       </td>
    </tr>
   @endforeach
  </tbody>
</table>







</div>
 @endsection