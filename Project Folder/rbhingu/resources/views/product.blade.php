@extends('layouts.main')
 @section('content')
 <div class= "container">
 @if($errors->any())
 @foreach($errors ->all() as $error)
 <div class="alert alert-danger" role="alert">
 {{ $error }}
</div>
@endforeach
 @endif
 
    <!-- Default form register -->
<form class="text-center border border-light p-5" action="{{ route('productstore') }}" method="POST" >
{{ csrf_field() }}

<p class="h4 mb-4">Sign up </p>

<div class="form-row mb-4">
    <div class="col">
        <!-- sku -->
        <input type="text" id="defaultRegisterFormFirstName" name="sku" class="form-control" placeholder="Stock Keeping Unit">
    </div>      
    

        <!--name -->
        <div class="col">
        <input type="text" id="defaultRegisterFormEmail" name="name" class="form-control mb-4" placeholder="Name">
        </div>
</div>
<div class="form-row mb-4">
<!-- category -->
    <div class="col">
        <input type="text" id="defaultRegisterPhonePassword" class="form-control" name="price" placeholder="Price" aria-describedby="defaultRegisterFormPhoneHelpBlock">
    </div>


        <!-- category -->
    <div class="col">
    <input type="text" id="defaultRegisterPhonePassword" class="form-control" name="category" placeholder="Category" aria-describedby="defaultRegisterFormPhoneHelpBlock">
    </div>
</div>

<div class="form-row mb-4">
    <div class="col">
    <input type="text" id="defaultRegisterPhonePassword" class="form-control" name="qty" placeholder="Quantity" aria-describedby="defaultRegisterFormPhoneHelpBlock">
    </div>

    <div class="col">
    <input type="file" id="defaultRegisterPhonePassword" class="form-control" name="image" placeholder="Image" aria-describedby="defaultRegisterFormPhoneHelpBlock">
    </div>
</div>   
<!-- Sign up button -->
<button class="btn btn-info my-4 btn-block" type="submit">Add Product</button>



</form>
</div>
<br>
<table class="table">
  <thead class="black white-text">
    <tr>
      <th scope="col">#</th>
      <th scope="col">SKU</th>
      <th scope="col">Name</th>
      <th scope="col">Price</th>
      <th scope="col">Category</th>
      <th scope="col">Quantity</th>
      <th scope="col">Image</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    

    @foreach($products as $product)
    <tr>
      <th scope="row">{{ $product->id }}</th>
      <td>{{ $product->sku }}</td>
      <td>{{ $product->Name }}</td>
      <td>{{ $product->Price }}</td>
      <td>{{ $product->Category }}</td>
      <td>{{ $product->Qty }}</td>
      <td>{{ $product->image }}</td>

      <td>
          <a class="btn btn-raised btn-primary btn-sm" href="{{ route('productedit',$product->id) }} ">
       <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
    
    <form method="post" id="delete-form-{{ $product->id }}" action="{{ route('productdelete',$product->id) }}" 
    style="display: none; "> 
    {{ csrf_field() }}
    {{ method_field('delete') }}
    </form>
    <button onclick="if (confirm('Are you sure to delete?')){
        event.preventDefault();
        document.getElementById('delete-form-{{$product->id}}').submit();
    }else{
        event.preventDefault();
    }
   

    "   class="btn btn-raised btn-danger btn-sm" href="  "><i class="fa fa-trash" aria-hidden="true">
       </i>
       </button>
       </td>
    </tr>
   @endforeach
  </tbody>
</table>

<!-- Default form register -->
 @endsection