<!-- Footer -->
<footer class="page-footer font-small info-color">



  <div style="background-color: info-color;">
    <div class="container">

      <!-- Grid row-->
      <div class="row py-4 d-flex align-items-center">

        <!-- Grid column -->
        <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
          <h6 class="mb-0">Get connected with us on social networks!</h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-6 col-lg-7 text-center text-md-right">

          <!-- Facebook -->
          <a class="fb-ic"  href="https://www.facebook.com/kiritbhai.hingu.7">
            <i class="fab fa-facebook-f white-text mr-4"> </i>
          </a>
      
          <!-- Google +-->
          <a class="gplus-ic">
            <i class="fab fa-google-plus-g white-text mr-4" > </i>
          </a>
          <!--Linkedin -->
        
          <!--Instagram-->
          <a class="ins-ic">
            <i class="fab fa-instagram white-text"> </i>
          </a>

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

    </div>
  </div>

  <!-- Footer Links -->
  <div class="container text-center text-md-left mt-5">

    <!-- Grid row -->
    <div class="row mt-3">

      <!-- Grid column -->
      <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

        <!-- Content -->
        <h6 class="text-uppercase font-weight-bold">Shop name</h6>
        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p>New R B Hingu Gents Tailors</p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

        <!-- Links -->
        <h6 class="text-uppercase font-weight-bold">Products</h6>
        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p>
          <a href="#!">Suits</a>
        </p>
        <p>
          <a href="#!">kurta Designs</a>
        </p>
        <p>
          <a href="#!">Pant Shirts</a>
        </p>
        <p>
          <a href="#!">Shervanis</a>
        </p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
     
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

        <!-- Links -->
        <h6 class="text-uppercase font-weight-bold">Contact</h6>
        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        
        <p>
          <i class="fas fa-home mr-3"></i> 
          <a href="https://www.google.com/maps/place/23%C2%B048'14.9%22N+72%C2%B023'14.7%22E/@23.8039454,72.3874463,20z/data=!4m5!3m4!1s0x0:0x0!8m2!3d23.804043!4d72.3874154">9/B.N Modi Chambers. Unjha-384170, Mehsana , Gujrat

        </a>
        </p>

        <p>
          <i class="fas fa-envelope mr-3"></i> rbhingu@gmail.com</p>
        <p>
          <i class="fas fa-phone mr-3"></i> +91 9898512305</p>
       
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href="https://mdbootstrap.com/"> MDBootstrap.com</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->