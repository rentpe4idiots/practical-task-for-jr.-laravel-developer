@extends('layouts.main')
 @section('content')
 <div class= "container">
 @if($errors->any())
 @foreach($errors ->all() as $error)
 <div class="alert alert-danger" role="alert">
 {{ $error }}
</div>
@endforeach
 @endif
 
    <!-- Default form register -->
<form class="text-center border border-light p-5" action="{{ route('store') }}" method="POST" >
{{ csrf_field() }}

<p class="h4 mb-4">Sign up </p>

<div class="form-row mb-4">
    <div class="col">
        <!-- First name -->
        <input type="text" id="defaultRegisterFormFirstName" name="Name" class="form-control" placeholder="Name of Customer">
    </div>      
    
</div>

<!-- E-mail -->
<input type="email" id="defaultRegisterFormEmail" name="Email" class="form-control mb-4" placeholder="E-mail">


<!-- Phone number -->
<input type="text" id="defaultRegisterPhonePassword" class="form-control" name="PhoneNo" placeholder="Phone number" aria-describedby="defaultRegisterFormPhoneHelpBlock">


<!-- Sign up button -->
<button class="btn btn-info my-4 btn-block" type="submit">Sign up</button>



</form>
</div>
<br>
<!-- Default form register -->
 @endsection