<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class MyController extends Controller
{
    public function index(){
        $customers = Customer::all();
        return view('welcome',compact('customers'));
    }
    
        public function login(){
          
            return view('login');
        }


    public function create(){
        return view('createCustomer');
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'Name' => 'required',
            'Email' => 'required',
            'PhoneNo' => 'required'
            
        ]);
        $customer = new Customer;
        $customer->customerName = $request->Name ;
        $customer->customerEmail = $request->Email ;
        $customer->phoneNo = $request->PhoneNo ;
        $customer->save();
        return redirect(route('home'))->with('message', 'Customer added succesfully');

    }
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('Edit',compact('customer'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'Name' => 'required',
            'Email' => 'required',
            'PhoneNo' => 'required'
            
        ]);
        $customer = Customer::find($id);
        $customer->customerName = $request->Name ;
        $customer->customerEmail = $request->Email ;
        $customer->phoneNo = $request->PhoneNo ;
        $customer->save();
        return redirect(route('home'))->with('message', 'Customer added updated');
    }
    public function delete($id)
    {
        Customer::find($id)->delete();
        return redirect(route('home'))->with('message', 'Record Deleted');
    }
}
