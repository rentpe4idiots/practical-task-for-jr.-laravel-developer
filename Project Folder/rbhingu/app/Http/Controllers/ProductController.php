<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\product;

class ProductController extends Controller
{
    public function index(){
       
            $products = product::all();
            return view('product',compact('products'));

    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'sku' => 'required',
            'name' => 'required',
            'price' => 'required',
            'category' => 'required',
            'qty' => 'required',
            'image' => 'required'
            
        ]);
        $product = new product;
        $product->sku = $request->sku ;
        $product->Name = $request->name ;
        $product->Price = $request->price ;
        $product->Category = $request->category ;
        $product->Qty = $request->qty ;
        $product->image = $request->image ;
        $product->save();
        return redirect(route('product'))->with('message', 'product added succesfully');

    }
    public function edit($id)
    {
        $product = product::find($id);
      
        return view('Editproduct',compact('product'));
        
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'sku' => 'required',
            'name' => 'required',
            'price' => 'required',
            'category' => 'required',
            'qty' => 'required',
            'image' => 'required'
            
        ]);
        $product = new product;
        $product->sku = $request->sku ;
        $product->Name = $request->name ;
        $product->Price = $request->price ;
        $product->Category = $request->category ;
        $product->Qty = $request->qty ;
        $product->image = $request->image ;
        $product->save();
        return redirect(route('product'))->with('message', 'product updated succesfully');



     }
    public function delete($id)
    {
        product::find($id)->delete();
        return redirect(route('home'))->with('message', 'Record Deleted');
    }
}
